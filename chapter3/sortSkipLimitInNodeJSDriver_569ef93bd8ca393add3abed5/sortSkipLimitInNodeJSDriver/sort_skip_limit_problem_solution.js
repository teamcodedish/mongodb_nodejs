var MongoClient = require('mongodb').MongoClient,
    commandLineArgs = require('command-line-args'), 
    assert = require('assert');


var options = commandLineOptions();


MongoClient.connect('mongodb://localhost:27017/school', function(err, db) {

    assert.equal(err, null);
    console.log("Successfully connected to MongoDB.");
    
    var query = queryDocument(options);
    var projection = {"student": 1};

    var cursor = db.collection('grades').find(query);
    cursor.project(projection);
    cursor.limit(options.limit);
    cursor.skip(options.skip);
    cursor.sort({"grade": 1});
        
    var numMatches = 0;

    cursor.forEach(
        function(doc) {
            numMatches = numMatches + 1;
            console.log(doc.student + "\n\t" );
        },
        function(err) {
            assert.equal(err, null);
            console.log("Our query was:" + JSON.stringify(query));
            console.log("Documents displayed: " + numMatches);
            return db.close();
        }
    );

});


function queryDocument(options) {

    console.log(options);
    
    var query = {};
    
    return query;
    
}


function commandLineOptions() {

    var cli = commandLineArgs([
        { name: "grade", alias: "g", type: Number , defaultValue:1},
        { name: "skip", alias: "s", type: Number, defaultValue: 0 },
        { name: "limit", alias: "l", type: Number, defaultValue: 20000 }
    ]);
    
    var options = cli.parse()

    return options;
    
}


